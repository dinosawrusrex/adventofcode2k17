def turns_to_escape_maze_1(jump_offset_list):
    list_copy = list(jump_offset_list)
    pointer = 0
    turn = 0
    while pointer >= 0 and pointer < len(list_copy):
        turn += 1
        current_index = pointer
        pointer += list_copy[pointer]
        list_copy[current_index] += 1
    return turn

def offset_index(value):
    if value > 2:
        return -1
    return 1

def turns_to_escape_maze_2(jump_offset_list):
    list_copy = list(jump_offset_list)
    pointer = 0
    turn = 0
    while pointer >= 0 and pointer < len(list_copy):
        turn += 1
        current_index = pointer
        pointer += list_copy[pointer]
        list_copy[current_index] += offset_index(list_copy[current_index])
    return turn

if __name__ == '__main__':
    with open('day5input.txt', 'r') as file:
        jump_offset_input = [int(line.strip()) for line in file]
    print("Result for part 1 is %d" % turns_to_escape_maze_1(jump_offset_input))
    print("Result for part 2 is %d" % turns_to_escape_maze_2(jump_offset_input))
