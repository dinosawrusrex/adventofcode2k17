if __name__ == '__main__':
    with open('day2input.txt', 'r') as file:
        sheet = [[int(number) for number in line.split()] for line in file]

    output_1 = sum(max(line) - min(line) for line in sheet)
    output_2 = sum([numerator/denominator
        for line in sheet for numerator in line for denominator in line
        if numerator > denominator and 0 not in [numerator, denominator]
        and denominator != 1])

    print('Result for part 1 is %d' % output_1)
    print('Result for part 2 is %d' % output_2)
